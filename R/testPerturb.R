#' Test a perturbation solution
#'
#' \code{testPerturb} Identifies a network and tests the simulation of the perturbation until it reaches an attractor
#' 
#' @param pkn Prior Knowledge Network
#' @param bool_data list of length two: first element is the starting, second is the desired phenotype
#' @param rule "ID" (default) or "MR"
#' @param perturbation Vector of transcription factors to perturb
#' @param percentage (default = FALSE)
#' @return Number of nodes perturbed if percentage == FALSE, otherwise percentage of perturbed nodes at the end of simulation
#' @family perturbation
testPerturb<- function(pkn, bool_data, rule, perturbation, percentage = FALSE){
  #infer network
  inferred_network = prune_gurobi(pkn, bool_data, rule)

  perturbed_network = inferred_network

  #induce the perturbation
  perturbed_bool_data = bool_data[[1]]
  for (i in 1:length(perturbation)){
    myTF = perturbation[i]
    if (myTF %in% perturbed_bool_data$TF) {
      perturbed_bool_data[myTF,'TF_value'] = 1-perturbed_bool_data[myTF,'TF_value']
    }
    perturbed_network = perturbed_network[perturbed_network$target!=myTF,]
  }

  #simulate
  res = simnet(perturbed_network, perturbed_bool_data, rule)
  colnames(res) = c('TF', 'result')
  res = merge (res, bool_data[[2]])
  num_perturbed = sum(res$result == res$TF_value)
  if(percentage) {
   num_perturbed = num_perturbed / length(unique(inferred_network$target))
  }

  #y <- .matchGenes2Adj(perturbed_bool_data,perturbed_network)

  #return(length(y[,1])/num)
  return(num_perturbed)

}



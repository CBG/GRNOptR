#' Convert Edge List to Adjacency Matrix
#'
#' \code{adj2el} converts a standard adjacency matrix to edge list
#'
#' @param adj Adjacency Matrix
#' @return Edge List
#' @family network conversion

adj2el <- function(adj){
  #empty frame for result set
  res = data.frame(TF=character(), effect=character(), target=character(), stringsAsFactors=FALSE)

  d = dim(adj)
  for (i in 1:d[1]){
    for (j in 1:d[2]){
      if(!adj[i,j]){
        #zero
        next
      }
      TF = colnames(adj)[j]
      target = rownames(adj)[i]
      if(adj[i,j]==1){ effect = "Activation"}
      else {
        if(adj[i,j]==-1){ effect = "Inhibition"}
        else{
          if(adj[i,j]==2){ effect = "Unspecified"}
        }
      }
      res = rbind(res, data.frame(TF,effect,target),stringsAsFactors = FALSE)
    }
  }
  res[] <- lapply(res, as.character)
  return(res)
}

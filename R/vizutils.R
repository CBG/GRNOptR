#' Plot a network
#'
#'
#'
#' @param el Edge List
#' @param bool_data Boolean data
#' @return tring to visualize
#' @family network visualization

plotNetwork <- function(el,bool_data=NULL){
  library("DOT")
  x = as.data.frame(sapply(el, function(n){ gsub("[^[:alnum:]]","", n) }) )
  dot(el2viz(x,bool_data))
}

#' Convert Edge List to Adjacency Matrix
#'
#' \code{el2viz} converts a standard edge list (e.g. geneA    Activation     geneB)
#' to an object to visualize
#'
#' @param el Edge List
#' @param bool_data Boolean data
#' @return string to visualize
#' @family network visualization

el2viz <- function(el,bool_data=NULL){
  strOut = 'digraph G {\n'
  strOut = paste0(strOut,
                  'graph [overlap=false,ranksep=1,layout=twopi]')
  arctxt = '->'
  for (i in 1:dim(el)[1]){

    strOut = paste0(strOut, '\n',
                    el[i,1], arctxt, el[i,3])

    if(tolower(el[i,2])=='inhibition' ){
      strOut = paste0(strOut, '[arrowhead="tee",color=red]')
    }

    if(tolower(el[i,2])=='unspecified' ){
      strOut = paste0(strOut, '[arrowhead="empty",color=gray]')
    }
  }

  if (!is.null(bool_data)){
    for (i in 1:dim(bool_data)[1]){

      if(bool_data[i,2]>0){
        strOut = paste0(strOut, '\n',
                        bool_data[i,1], '[color=blue]')
      }else {
        strOut = paste0(strOut, '\n',
                        bool_data[i,1], '[color=red]')
      }
    }
  }

  strOut = paste0(strOut, '\n','}')
  return(strOut)
}

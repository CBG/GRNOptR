#' Package GRNOpt
#'
#' Gene Regulatory Network Inference Using Optimization
#'
#' \code{GRNOpt} infers Gene Regulatory Network (GRN) from booleanized transcriptomic data and Prior Knowledge Network (PKN) using optmization methods. The package also includes methods for simulation and visualization.
#'
#' @name GRNOpt
#' @docType package
"_PACKAGE"

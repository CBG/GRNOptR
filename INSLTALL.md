# Seesaw Installation Guide



## install gurobi
See instructions here:
http://www.gurobi.com/documentation/7.5/quickstart_linux.pdf

Check compatibility with gurobi:
http://www.gurobi.com/products/supported-platforms
(7.5 is compatible with R 3.4)

e.g.:
```bash
cd /tmp
wget http://packages.gurobi.com/7.5/gurobi7.5.2_linux64.tar.gz
tar xvfz gurobi7.5.2_linux64.tar.gz
mv gurobi752 /opt/
grbgetkey ...
```

insert this into .bashrc:

```
export           GUROBI_HOME="/opt/gurobi752/linux64"
export           PATH="${PATH}:${GUROBI_HOME}/bin"
export           LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${GUROBI_HOME}/lib"
```

## install gurobi in R
(as user)
```R
install.packages('slam')
install.packages('/opt/gurobi752/linux64/R/gurobi_7.5-2_R_x86_64-pc-linux-gnu.tar.gz', repos=NULL)
```

### check gcc version
```bash
gcc -v
```
IMPORTANT:
Use libgurobi_g++5.2.a for C++ in Ubuntu 16.04, 16.10, and 17.04
```bash
ln -s libgurobi_g++5.2.a libgurobi_c++.a 
```

### install rgurobi package
```R
if (!require('devtools'))
        install.packages('devtools', repo='http://cran.rstudio.com', dep=TRUE)
devtools:::install_github('paleo13/rgurobi')
```

## install GRNOpt

### generate ssh key-pair
```bash
mkdir ~/.ssh
chmod 700 ~/.ssh
ssh-keygen -t rsa
```

public key had to be added to the repository LUMS
### install LibSSH2
```bash
sudo apt-get install libssh2-1 libssh2-1-dev
```

### Check ssh
(within R)
```R
#If ssh is not active then libssh has to be installed, then re-install git2r
#sudo apt-get install libssh2-1 libssh2-1-dev
within R
library(git2r)
libgit2_features()
library(git2r)
libgit2_features()
```

```R
cred = git2r::cred_ssh_key(publickey = "/home/shane/.ssh/id_rsa.pub", privatekey = "/home/shane/.ssh/id_rsa")
devtools::install_git("ssh://git@git-r3lab-server.uni.lu:8022/andras.hartmann/Logic_Pipeline.git", subdir="GRNOpt", branch="master", credentials = cred)
```


# GRNOpt package for R
Gene Regulatory Network Inference Using Optimization

GRNOpt infers Gene Regulatory Network (GRN) from booleanized transcriptomic data and Prior Knowledge Network (PKN) using optmization methods. The package also includes methods for simulation and visualization.



## Installation manual 

### Dependencies
- R (>=3.4)
- gurobi r package (>=8.0.0)

New version at https://www.gurobi.com/downloads/gurobi-software/  

 
If not sudo permissions, extract gurobi at home (or whatever directory you will point when editing .bashrc later) 


Get license from (requires academic IP, achieved with cisco anyconnect):  

https://portal.gurobi.com/iam/licenses/request  


**CHANGE FROM VERSIONS 9 ONWARDS**

You only need the license file (gurobi.lic) downloaded in a path the bash environment will find:  

Example for gurobi v10.0.1

```bash
$ export GUROBI_HOME="/home/<(superuser if any)/(user)>/gurobi1001/linux64" 
$ export PATH="${PATH}:${GUROBI_HOME}/bin" 
$ export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${GUROBI_HOME}/lib" 
$ export GRB_LICENSE_FILE='/home/<(superuser if any)/(user)>/gurobi.lic' 
```

This will automatically make within R 

```R
>install.packages('/home/<(superuser if any)/(user)>/gurobi1001/linux64/R/gurobi_10.0-1_R_4.2.0.tar.gz', repos=NULL) 
```

And with the enviroment variables, it should work. But to make this permanent you need to edit your .bashrc file. Otherwise it will only work if before starting R (or Rstudio) in a terminal you put them commands above.

To install the repository in R you can use 

```R
> install.packages("devtools")
> devtools::install_git("https://gitlab.lcsb.uni.lu/CBG/GRNOptR.git", ref ="master")
```
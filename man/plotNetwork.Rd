% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/vizutils.R
\name{plotNetwork}
\alias{plotNetwork}
\title{Plot a network}
\usage{
plotNetwork(el, bool_data = NULL)
}
\arguments{
\item{el}{Edge List}

\item{bool_data}{Boolean data}
}
\value{
tring to visualize
}
\description{
Plot a network
}
\seealso{
Other network visualization: \code{\link{el2viz}}
}

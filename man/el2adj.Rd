% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/el2adj.R
\name{el2adj}
\alias{el2adj}
\title{Convert Edge List to Adjacency Matrix}
\usage{
el2adj(el)
}
\arguments{
\item{el}{Edge List}
}
\value{
Adjacency Matrix
}
\description{
\code{el2adj} converts a standard edge list (e.g. geneA    Activation     geneB)
to an adjacencey matrix (of class matrix)
}
\seealso{
Other network conversion: \code{\link{adj2el}},
  \code{\link{el2sif}}, \code{\link{sif2el}}
}
